#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Desarrolla un programa que permita ingresar una cadena
(un string) y evalúe si es un correo electrónico, un entero,
un real u otra cosa.
"""

import sys


def es_correo_electronico(string):
    if "@" in string and string.find("@") > 0:
        for i in range(string.find("@") + 1, len(string)):
            if string[i] == ".":
                return True

    else:
        return False

def es_entero(string):
    try:
        int(string)
        return True
    except ValueError:
        return False

def es_real(entrada):
    try:
        float(entrada)
        return True
    except ValueError:
        return False


def evaluar_entrada(string):
    if string == "":
        print("no ha ingresado ningún string")
        return None
    else:
        if es_correo_electronico(string):
            return ("es un correo ectronico")
        elif es_entero(string):
            return (" es un entero ")
        elif es_real(string):
            return("es un numero real")
        else:
            return ("no es ni un correo, ni un entero, ni un real")


def main():
    if len(sys.argv) < 2:
        sys.exit("Error: se espera al menos un argumento")
    string = sys.argv[1]
    resultado = evaluar_entrada(string)
    print(resultado)

if __name__ == '__main__':
    main()
